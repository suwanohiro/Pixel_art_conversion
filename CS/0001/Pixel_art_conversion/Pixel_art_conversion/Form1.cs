﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pixel_art_conversion
{
    public partial class Form1 : Form
    {
        Bitmap bitmap;
        Image_function imf;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string src = "C:\\Users\\super\\OneDrive\\Storage\\Windows\\TUF Gaming F17 (FX706HM-HX073T)\\ダウンロード\\maxresdefault.jpg";
            imf = new Image_function();
            imf.Set_ImgPass(src);
            //bitmap = new Bitmap(@"C:\Users\super\OneDrive\Storage\Windows\TUF Gaming F17 (FX706HM-HX073T)\ダウンロード\maxresdefault.jpg");

            pictureBox1.Image = bitmap;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pass = "C:\\Users\\super\\OneDrive\\Storage\\Windows\\TUF Gaming F17 (FX706HM-HX073T)\\仮置場\\2022-08-12\\test.png";
            output(bitmap, pass);
        }

        private void output(Bitmap data, string pass)
        {
            ImageFormat type = System.Drawing.Imaging.ImageFormat.Png;
            data.Save(@pass, type);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            imf.PreView_Img();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            imf.PreViewClear();
        }
    }

    class Image_function {
        private string img_pass = "";
        private ImageFormat output_type = System.Drawing.Imaging.ImageFormat.Png;
        private Bitmap bmp;
        private Image_PreView_Display IPD;

        public Image_function()
        {
            IPD = new Image_PreView_Display(this.bmp);
        }

        public void Set_ImgPass(string str)
        {
            this.img_pass = str;
        }

        public string Get_ImgPass()
        {
            return this.img_pass;
        }

        public void Img_output(string output_src)
        {
            this.bmp.Save(@output_src, this.output_type);
        }

        public void PreView_Img()
        {
            IPD.Hide();
            this.bmp = new Bitmap(this.Get_ImgPass());
            IPD = new Image_PreView_Display(this.bmp);
            IPD.Show();
        }

        public void PreViewClear()
        {
        }

        public partial class Image_PreView_Display : Form
        {
            private System.Windows.Forms.PictureBox pictureBox1;
            private Bitmap bitmap;

            public Image_PreView_Display(Bitmap bmp)
            {
                this.bitmap = bmp;
                InitializeComponent();
            }

            private void InitializeComponent()
            {
                this.Text = "Image_PreView_Display";
                this.Width = 1280;
                this.Height = 720;
                this.Load += new System.EventHandler(this.Image_PreView_Display_Load);
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

                //pictureBox
                this.pictureBox1 = new System.Windows.Forms.PictureBox();
                this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left)
                | System.Windows.Forms.AnchorStyles.Right)));
                this.pictureBox1.Location = new System.Drawing.Point(0, 0);
                this.pictureBox1.Name = "pictureBox1";
                this.pictureBox1.Size = new System.Drawing.Size(1264, 681);
                this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                this.pictureBox1.TabIndex = 0;
                this.pictureBox1.TabStop = false;
                this.Controls.Add(this.pictureBox1);
            }

            private void Image_PreView_Display_Load(object sender, EventArgs e)
            {
                this.Text = "asd";
                pictureBox1.Image = this.bitmap;
            }
        }
    }
}
